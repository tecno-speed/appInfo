document.addEventListener('deviceready', onDeviceready)

function onDeviceready() {
    document.addEventListener('offline', onOffline)
    document.addEventListener('online', onOnline)

    // Pega informações da Rede
    retornaStatus();

    // Pega informações do Device
    var msgDevice = '';
    msgDevice += '<br>' + device.cordova;
    msgDevice += '<br>' + device.model;
    msgDevice += '<br>' + device.platform;
    msgDevice += '<br>' + device.uuid;
    msgDevice += '<br>' + device.version;
    msgDevice += '<br>' + device.manufacturer;
    msgDevice += '<br>' + device.isVirtual;
    msgDevice += '<br>' + device.serial;

    document.getElementById('device').innerHTML = msgDevice;

    // Pega informações do Acelerometro
    var options = { frequency: 1000 }
    navigator.accelerometer.watchAcceleration(onSuccess, onError, options);
}

function onSuccess(acceleration) {
    document.getElementById('motionX').innerHTML = acceleration.x;
    document.getElementById('motionY').innerHTML = acceleration.y;
    document.getElementById('motionZ').innerHTML = acceleration.z;
}

function onError() {
    alert('Deu erro')
}

function retornaStatus() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';

    document.getElementById('network').innerHTML = states[networkState];
}

function onOffline() {
    alert('Sua rede está off-line')
}

function onOnline() {
    alert('Sua rede está on-line')
    retornaStatus();
}

var media;

function gravar() {
    console.log('Começou a gravação')
    media = new Media('aula1.mp3')
    media.startRecord();
}

function stop() {
    console.log('Parou a gravação')
    media.stopRecord();
}

function play() {
    console.log('Tocou a gravação')
    media.play();
}
